﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TestTaskSymodSoft.Dto;
using TestTaskSymodSoft.DTO;
using TestTaskSymodSoft.Model;

namespace TestTaskSymodSoft.Configurations
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<Role, RoleDTO>();
            CreateMap<RoleDTO, Role>();
            CreateMap<RolePostDTO, Role>();
            CreateMap<User, UserDTO>().ForMember(
                dto => dto.Roles,
                user => user.MapFrom(sour => sour.UserRoles.Select(rol => rol.Role).ToList()));
            CreateMap<UserDTO, User>();
            CreateMap<UserPostDTO, User>();
            CreateMap<Project, ProjectDTO>().ForMember(
                dto => dto.Users,
                user => user.MapFrom(
                    sour => sour.UserProjects.Select(i => i.UserId).AsEnumerable())
                );
            CreateMap<ProjectDTO, Project>();
            CreateMap<ProjectPostDTO, Project>();
        }
    }
}
