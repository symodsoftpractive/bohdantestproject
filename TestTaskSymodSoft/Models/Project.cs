﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestTaskSymodSoft.Model
{
    public partial class Project
    {
        public Project()
        {
        }

        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        

        public DateTime CreateDate { get; set; }

        public virtual ICollection<UserProjects> UserProjects { get; set; }
    }
}
