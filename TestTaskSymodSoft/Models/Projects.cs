﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestTaskSymodSoft.Model
{
    public partial class Projects
    {
        public Projects()
        {
            ProjectsHasUser = new HashSet<ProjectsHasUser>();
        }

        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        

        public DateTime CreateDate { get; set; }

        public virtual ICollection<ProjectsHasUser> ProjectsHasUser { get; set; }
    }
}
