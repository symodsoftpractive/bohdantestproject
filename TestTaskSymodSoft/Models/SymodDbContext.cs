﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using TestTaskSymodSoft.Dto;

namespace TestTaskSymodSoft.Model
{
    public partial class SymodDbContext : DbContext
    {
        public SymodDbContext()
        {
        }

        public SymodDbContext(DbContextOptions<SymodDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<UserProjects> UserProjects { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserRoles> UsersRoles { get; set; }


        
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);


    }
}
