﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestTaskSymodSoft.Model
{
    public partial class ProjectsHasUser
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int UserId { get; set; }

        public virtual Projects Project { get; set; }
        public virtual User User { get; set; }
    }
}
