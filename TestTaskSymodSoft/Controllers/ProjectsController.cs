﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestTaskSymodSoft.Dto;
using TestTaskSymodSoft.DTO;
using TestTaskSymodSoft.Model;

namespace TestTaskSymodSoft.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly SymodDbContext _context;
        private readonly IMapper _mapper;

        public ProjectsController(SymodDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Projects
        [HttpGet]
        public async Task<IEnumerable<ProjectsDTO>> GetProjects()
        {
            IEnumerable<Projects> Projectss = await _context.Projects.Include(pr => pr.ProjectsHasUser).ToListAsync();
            IEnumerable<ProjectsDTO> ProjectssDTO = _mapper.Map<IEnumerable<ProjectsDTO>>(Projectss);
            return ProjectssDTO;
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectsDTO>> GetProjects(int id)
        {
            Projects Projects = await _context.Projects.Include(pr => pr.ProjectsHasUser).Where(pr => pr.Id == id).SingleAsync();

            if (Projects == null)
            {
                return NotFound();
            }

            ProjectsDTO resultDto = _mapper.Map<ProjectsDTO>(Projects);
            return resultDto;
        }

        // PUT: api/Projects/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProjects(int id, ProjectsDTO ProjectsDTO)
        {
            if (id != ProjectsDTO.Id)
            {
                return BadRequest();
            }
            Projects Projects = _mapper.Map<Projects>(ProjectsDTO);
            _context.Entry(Projects).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjectsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Projects
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ProjectsDTO>> PostProjects(ProjectsPostDTO projectsPostDTO)
        {
            Projects projects = _mapper.Map<Projects>(projectsPostDTO);
           
            _context.Projects.Add(projects);
            await _context.SaveChangesAsync();
            ProjectsDTO result = _mapper.Map<ProjectsDTO>(projects);
            return CreatedAtAction("GetProjects", new { id = projects.Id }, result);
        }

        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProjectsDTO>> DeleteProjects(int id)
        {
            Projects Projects = await _context.Projects.Include(pr => pr.ProjectsHasUser).Where(pr => pr.Id == id).FirstAsync();
            if (Projects == null)
            {
                return NotFound();
            }

            _context.Projects.Remove(Projects);
            await _context.SaveChangesAsync();

            ProjectsDTO ProjectsDTO = _mapper.Map<ProjectsDTO>(Projects);
            return ProjectsDTO;
        }
        [HttpPost("{projectId}/users/{userId}")]
        public async Task<ActionResult> AddRoleToUser(int projectId, int userId)
        {
            Projects projects = await _context.Projects.FindAsync(projectId);
            if (projects == null)
            {
                return NotFound("Project by id:" + projectId + " not found.");
            }
            User user = await _context.User.FindAsync(userId);
            if (user == null)
            {
                return NotFound("User by id:" + userId + " not found.");
            }
            if (ProjectsHasUser(projectId, userId))
            {
                return BadRequest("Project has user with id:" + userId);
            }
            ProjectsHasUser row = new ProjectsHasUser
            {
                UserId = userId,
                ProjectId = projectId
            };

            _context.ProjectsHasUser.Add(row);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{projectId}/users/{userId}")]
        public async Task<ActionResult> RemoveUserFromProject(int projectId, int userId)
        {
            ProjectsHasUser row = await _context.ProjectsHasUser.Where(i => i.UserId == userId && projectId == i.ProjectId).SingleAsync();
            if (row == null)
            {
                return NotFound();
            }

            _context.ProjectsHasUser.Remove(row);
            await _context.SaveChangesAsync();
            return Ok();
        }

        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.Id == id);
        }

        private bool ProjectsHasUser(int projectId, int userId)
        {
            return _context.ProjectsHasUser.Any(e => e.UserId == userId && e.ProjectId == projectId);
        }

        private bool ProjectsExists(int id)
        {
            return _context.Projects.Any(e => e.Id == id);
        }

    }
}
