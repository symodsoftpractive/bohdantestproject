﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestTaskSymodSoft.Dto;
using TestTaskSymodSoft.DTO;
using TestTaskSymodSoft.Model;

namespace TestTaskSymodSoft.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly SymodDbContext _context;
        private readonly IMapper _mapper;

        public UserController(SymodDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/User
        [HttpGet]
        public async Task<IEnumerable<UserDTO>> GetAll()
        {
            IEnumerable<User> users = await _context.Users.Include(data => data.UserRoles)
                .ThenInclude(t => t.Role).ToListAsync();
            IEnumerable<UserDTO> usersDTO = _mapper.Map<IEnumerable<UserDTO>>(users);
            return usersDTO;
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetUser(int id)
        {
            User user = await _context.Users.Include(u => u.UserRoles)
                .ThenInclude(t => t.Role).Where(us=>us.Id == id).FirstAsync();
           
            if (user == null)
            {
                return NotFound();
            }

            UserDTO result = _mapper.Map<UserDTO>(user);
            return result;
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser([FromRoute] int id, [FromBody] UserDTO userDTO)
        {
            if (id != userDTO.Id)
            {
                return BadRequest();
            }
            if (!UserExists(id))
            {
                return NotFound();
            }


            User user = _mapper.Map<User>(userDTO);
            _context.Entry(user).State = EntityState.Modified;
             await _context.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/User
        [HttpPost]
        public async Task<ActionResult<UserDTO>> PostUser([FromBody] UserPostDTO userPostDTO)
        {
            User user = _mapper.Map<User>(userPostDTO);

            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            UserDTO result = _mapper.Map<UserDTO>(user);

            return CreatedAtAction("GetUser", new { id = user.Id }, result);
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UserDTO>> DeleteUser(int id)
        {
            User user = await _context.Users.Include(u => u.UserRoles)
                .ThenInclude(t => t.Role).Where(us => us.Id == id).SingleAsync();
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            UserDTO userDTO = _mapper.Map<UserDTO>(user);
            return userDTO;
        }


        [HttpPost("{userId}/role/{roleId}")]
        public async Task<ActionResult> AddRoleToUser([FromRoute] int userId,[FromRoute] int roleId)
        {
            if (!UserExists(userId))
            {
                return NotFound();
            }

            if (!RoleExists(roleId))
            {
                return NotFound();
            }

            if (UserHasRole(userId, roleId))
            {
                return BadRequest("User has role with id:" + roleId);
            }

            UserRoles row = new UserRoles
            {
                UserId = userId,
                RoleId = roleId
            };

            _context.UsersRoles.Add(row);
            await _context.SaveChangesAsync();
            return Ok();
        }
        
        [HttpDelete("{userId}/role/{roleId}")]
        public async Task<ActionResult> RemoveRoleFromUser([FromRoute] int userId, [FromRoute] int roleId)
        {

            UserRoles row = await _context.UsersRoles.Where(i => i.UserId == userId && roleId == i.RoleId).FirstAsync();
            if(row == null)
            {
                return NotFound();
            }

            _context.UsersRoles.Remove(row);
            await _context.SaveChangesAsync();
            return Ok();
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
        private bool RoleExists(int id)
        {
            return _context.Roles.Any(e => e.Id == id);
        }

        private bool UserHasRole(int userId,int roleId)
        {
            return _context.UsersRoles.Any(e => e.UserId == userId && e.RoleId == roleId);
        }

    }
}
