﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestTaskSymodSoft.Dto;
using TestTaskSymodSoft.DTO;
using TestTaskSymodSoft.Model;

namespace TestTaskSymodSoft.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly SymodDbContext _context;
        private readonly IMapper _mapper;

        public ProjectController(SymodDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        public async Task<IEnumerable<ProjectDTO>> GetAll()
        {
            IEnumerable<Project> projects = await _context.Projects.Include(val => val.UserProjects).ToListAsync();
            IEnumerable<ProjectDTO> projectsDTO = _mapper.Map<IEnumerable<ProjectDTO>>(projects);
            return projectsDTO;
        }

        // GET: api/Project/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetProject([FromRoute] int id)
        {
            Project project = await _context.Projects.Include(pr => pr.UserProjects)
                .Where(pr => pr.Id == id).SingleAsync();

            if (project == null)
            {
                return NotFound();
            }

            ProjectDTO result = _mapper.Map<ProjectDTO>(project);

            return result;
        }

        // PUT: api/Project/5
        [HttpPut("{id}")]
        public async Task<ActionResult<ProjectDTO>> PutProject([FromRoute] int id,[FromBody] ProjectDTO projectDTO)
        {
            if (id != projectDTO.Id)
            {
                return BadRequest();
            }
            if (!ProjectExists(id))
            {
                return NotFound();
            }
            Project project = _mapper.Map<Project>(projectDTO);
            _context.Entry(project).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            
            return NoContent();
        }

        // POST: api/Projects
        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> PostProject([FromBody] ProjectPostDTO projectPostDTO)
        {
            Project project = _mapper.Map<Project>(projectPostDTO);

            _context.Projects.Add(project);
            await _context.SaveChangesAsync();
            ProjectDTO result = _mapper.Map<ProjectDTO>(project);
            return CreatedAtAction("GetProject", new { id = project.Id }, result);
        }

        // DELETE: api/Project/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProjectDTO>> DeleteProjects(int id)
        {
            Project project = await _context.Projects.Include(pr => pr.UserProjects)
                .Where(pr => pr.Id == id).FirstAsync();
            if (project == null)
            {
                return NotFound();
            }

            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();

            ProjectDTO result = _mapper.Map<ProjectDTO>(project);
            return result;
        }
        [HttpPost("{projectId}/users/{userId}")]
        public async Task<ActionResult> AddRoleToUser([FromRoute] int projectId, [FromRoute] int userId)
        {
            if (!ProjectExists(projectId))
            {
                return NotFound("Project by id:" + projectId + " not found.");
            }
            if (!UserExists(userId))
            {
                return NotFound("User by id:" + userId + " not found.");
            }
            if (ProjectHasUser(projectId, userId))
            {
                return BadRequest("Project has user with id:" + userId);
            }
            UserProjects row = new UserProjects
            {
                UserId = userId,
                ProjectId = projectId
            };

            _context.UserProjects.Add(row);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{projectId}/users/{userId}")]
        public async Task<ActionResult> RemoveUserFromProject([FromRoute] int projectId, [FromRoute] int userId)
        {
            UserProjects row = await _context.UserProjects
                .Where(i => i.UserId == userId && projectId == i.ProjectId).FirstAsync();
            if (row == null)
            {
                return NotFound();
            }

            _context.UserProjects.Remove(row);
            await _context.SaveChangesAsync();
            return Ok();
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        private bool ProjectHasUser(int projectId, int userId)
        {
            return _context.UserProjects.Any(e => e.UserId == userId && e.ProjectId == projectId);
        }

        private bool ProjectExists(int id)
        {
            return _context.Projects.Any(e => e.Id == id);
        }

    }
}
