﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestTaskSymodSoft.Dto;
using TestTaskSymodSoft.DTO;
using TestTaskSymodSoft.Model;

namespace TestTaskSymodSoft.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly SymodDbContext _context;
        private readonly IMapper _mapper;

        public RoleController(SymodDbContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Role
        [HttpGet]
        public async Task<IEnumerable<RoleDTO>> GetAll()
        {
            IEnumerable<Role> roles = await _context.Roles.ToListAsync();
            IEnumerable<RoleDTO> rolesDTO = _mapper.Map<IEnumerable<RoleDTO>>(roles);
            return rolesDTO;
        }

        // GET: api/Role/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RoleDTO>> GetRole(int id)
        {
            Role role = await _context.Roles.FindAsync(id);

            if (role == null)
            {
                return NotFound();
            }

            RoleDTO result = _mapper.Map<RoleDTO>(role);
            return result;
        }

        // PUT: api/Role/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRole([FromRoute] int id, [FromBody] RoleDTO roleDTO)
        {
            if (id != roleDTO.Id)
            {
                return BadRequest();
            }
            if (!RoleExists(id))
            {
                return NotFound();
            }
            Role role = _mapper.Map<Role>(roleDTO);
            _context.Entry(role).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Role
        [HttpPost]
        public async Task<ActionResult<RoleDTO>> PostRole([FromBody] RolePostDTO rolePostDTO)
        {
            Role role = _mapper.Map<Role>(rolePostDTO);
            if (RoleExistsByName(role.Name))
            {
                return BadRequest("Role with name:"+ role.Name + "exist!");
            }
            _context.Roles.Add(role);
            await _context.SaveChangesAsync();
            RoleDTO result = _mapper.Map<RoleDTO>(role);
            return CreatedAtAction("GetRole", new { id = role.Id }, result);
        }

        // DELETE: api/Role/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RoleDTO>> DeleteRole(int id)
        {
            Role role = await _context.Roles.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }

            _context.Roles.Remove(role);
            await _context.SaveChangesAsync();

            RoleDTO result = _mapper.Map<RoleDTO>(role);
            return result;
        }

        private bool RoleExists(int id)
        {
            return _context.Roles.Any(e => e.Id == id);
        }
        private bool RoleExistsByName(string name)
        {
            return _context.Roles.Any(e => e.Name.Equals(name));
        }
    }
}
