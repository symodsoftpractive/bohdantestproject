﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskSymodSoft.Model;

namespace TestTaskSymodSoft.DTO
{
    public class ProjectPostDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }

    }
}
