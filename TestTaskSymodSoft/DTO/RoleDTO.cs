﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTaskSymodSoft.Dto
{
    public class RoleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
       
    }
}
