﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTaskSymodSoft.DTO
{
    public class RolePostDTO
    {
        public string Name { get; set; }
    }
}
