﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskSymodSoft.Dto;
using TestTaskSymodSoft.Model;

namespace TestTaskSymodSoft.DTO
{
    public class UserPostDTO
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string Password { get; set; }
        public string LastName { get; set; }
        public DateTime? Birthday { get; set; }
        public string Phone { get; set; }
    }
}
